import React  from 'react';
import ReactDOM  from 'react-dom';
import { browserHistory, Router, Route, IndexRoute, Link, IndexLink } from 'react-router';

import Main from './components/Main';

const routes = (
  <Router history={browserHistory}>
    <Route path="/" component={Main}>
      <Route path="*" component={Main} />
    </Route>
  </Router>
);

ReactDOM.render(routes, document.querySelector('#app'));
