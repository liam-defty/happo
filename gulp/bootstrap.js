'use strict';

const gulp = require('gulp');
const gutil = require('gulp-util');
const gulpLoadPlugins = require('gulp-load-plugins');
const config = require('./config');

// Override NODE_ENV when using `gulp <task> --production`.
config.ENV = gutil.env.production ? 'production' : 'dev';

// Sync NODE_ENV to gulp env.
process.env.NODE_ENV = config.ENV;

// Load in plugins based on env.
const plugins = gulpLoadPlugins({
  scope: config.ENV === 'production' ? ['dependencies'] : ['dependencies', 'devDependencies']
});

// Non-gulp plugins required on all environments.
plugins.del = require('del');
plugins.env = require('node-env-file');

// Non-gulp plugins only required on dev environment.
if (config.ENV === 'dev') {
  plugins.browserSync = require('browser-sync').create();
  plugins.pngquant = require('imagemin-pngquant');
  plugins.sassdoc = require('sassdoc');
}

// Require each task, pass in gulp, plugins and config.
config.tasks.forEach(task => {
  require('./tasks/' + task)(gulp, plugins, config);
});
