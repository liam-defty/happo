'use strict';

module.exports = (gulp, plugins, config) => {
  const paths = config.paths.livereload;

  gulp.task('browsersync', ['watch'], () => {
    config.isBrowserSync = true;

    const browserSyncProxy = process.env.BROWSERSYNC_PROXY;

    let browserSyncConfig = {
      open: plugins.util.env.open || false,
      notify: plugins.util.env.notify || false,
    }

    if (browserSyncProxy === undefined || browserSyncProxy === 'false') {
      browserSyncConfig.server = {
        baseDir: config.dist
      }
    } else {
      browserSyncConfig.proxy = browserSyncProxy;
    }

    plugins.browserSync.init(browserSyncConfig);
  });
};
