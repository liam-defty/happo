'use strict';

module.exports = (gulp, plugins, config) => {
  const paths = config.paths.clean;

  gulp.task('clean', () => {
    return plugins.del(config.ENV === 'dev' ? paths.dev : paths.production);
  });
};
