'use strict';

module.exports = (gulp, plugins, config) => {
  const paths = config.paths.favicons;
  const options = paths.options; // Just for semantics

  gulp.task('favicons:clean', () => {
    return plugins.del(paths.out);
  });

  gulp.task('favicons', ['favicons:clean'], () => {
    return gulp.src(paths.src)
      .pipe(plugins.favicons(options))
      .pipe(gulp.dest(paths.srcDir))
  });
};
