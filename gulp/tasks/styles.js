'use strict';

const transforms = require('../common/postcssTransforms');

module.exports = (gulp, plugins, config) => {
  const paths = config.paths.styles;

  gulp.task('styles:lint', () => {
    const scss = require('postcss-scss');

    let lintTransforms = [
      require('stylelint')({
        "rules": {
          "declaration-colon-space-after": [2, "always"],
          "declaration-colon-space-before": [2, "never"],
          "no-multiple-empty-lines": 2,
          "number-leading-zero": [2, "never"],
          "rule-no-duplicate-properties": 2,
          "string-quotes": [2, "double"],
          "block-no-empty": 2,
          "rule-trailing-semicolon": [2, "always"],
          "block-closing-brace-newline-after": [2, "always"],
          "block-opening-brace-space-before": [2, "always"],
          "color-no-invalid-hex": 2,
          "color-hex-case": [2, "lower"],
          "declaration-bang-space-before": [2, "always"],
          "no-missing-eof-newline": 2,
          "number-leading-zero": [2, "never"],
          "number-no-trailing-zeros": 2,
          "selector-pseudo-element-colon-notation": [2, "double"],
          // "comment-empty-line-before": [2, "always"], -> Need to figure this one as it doesn't like Sass comments.
        }
      }),
     require('postcss-reporter')({
        clearMessages: true,
      })
    ];

    return gulp.src(paths.lint)
      .pipe(plugins.postcss(lintTransforms, { parser: scss }))
  });

  // gulp.task('styles', ['styles:lint'], () => {
  gulp.task('styles', () => {
    return gulp.src(paths.src)
      .pipe(plugins.plumber())
      .pipe(config.ENV === 'dev' ? plugins.sourcemaps.init() : plugins.util.noop())
      .pipe(plugins.sass.sync({
        includePaths: paths.includePaths
      }).on('error', plugins.sass.logError))
      .pipe(plugins.postcss(transforms))
      .pipe(config.ENV === 'dev' ? plugins.sourcemaps.write('.') : plugins.util.noop())
      .pipe(gulp.dest(paths.out));
  });
};
