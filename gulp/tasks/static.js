'use strict';

const transforms = require('../common/postcssTransforms');

module.exports = (gulp, plugins, config) => {
  const paths = config.paths.static;

  gulp.task('static', () => {

    // Filters
    const cssFilter = plugins.filter('**/*.css', { restore: true });

    return gulp.src(paths.src)
      .pipe(plugins.plumber())
      .pipe(plugins.changed(paths.out))

      // .css filter
      .pipe(cssFilter)
      .pipe(plugins.postcss(transforms))
      .pipe(cssFilter.restore)

      .pipe(gulp.dest(paths.out));
  });
};
