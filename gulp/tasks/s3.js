'use strict';

module.exports = (gulp, plugins, config) => {

  // Grab data from .env file. Assigned to `process.env`.
  plugins.env('.env');

  // Create awspublish `config` object.
  const publisher = plugins.awspublish.create({
    params: {
      Bucket: process.env.S3_BUCKET,
    },
    accessKeyId: process.env.S3_KEY,
    secretAccessKey: process.env.S3_SECRET,
    region: process.env.S3_REGION
  });

  // The task.
  gulp.task('s3:publish', () => {

    // Set up .css/.js filter for gzipping.
    const gzipFilter = plugins.filter(['**/*.css', '**/*.js']);

    // gulp.src is the files to sync to s3.
    return gulp.src(config.s3.local)

      // Set up a rename to the location on s3.
      .pipe(plugins.rename(path => {
        path.dirname = config.s3.mapTo + path.dirname;
      }))

      // Gzip filter.
      .pipe(gzipFilter)
      .pipe(plugins.awspublish.gzip())
      .pipe(gzipFilter.restore())

      // Create cache file for speed.
      .pipe(publisher.cache())

      // Publish files.
      .pipe(publisher.publish())

      // Sync files to delete any left over s3 files that have been deleted locally.
      .pipe(publisher.sync('assets'))

      // Reports are useful!
      .pipe(plugins.awspublish.reporter());
  });
};

/**

Needed s3 policy:

{
  "Version": "2008-10-17",
  "Id": "Policy1426886760893",
  "Statement": [
    {
      "Sid": "Stmt1426886751211",
      "Effect": "Allow",
      "Principal": "*",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject",
        "s3:ListMultipartUploadParts",
        "s3:AbortMultipartUpload",
        "s3:ListBucket"
      ],
      "Resource": "arn:aws:s3:::assets.bigbitecreative.com/*"
    }
  ]
}
*/
