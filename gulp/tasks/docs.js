'use strict';

module.exports = (gulp, plugins, config) => {
  const paths = config.paths.docs;

  gulp.task('docs', ['sassdoc', 'styleguide'], () => {
    return gulp.src(paths.src)
      .pipe(plugins.webserver({
        livereload: true,
        port: 8082,
        open: false
      }));
  });

  /**
   * Move compiled assets to docs folder.
   */
  gulp.task('styleguide', ['styleguide:clean'], () => {
    return gulp.src('./public/assets/**')
      .pipe(gulp.dest('./docs/styleguide/assets'));
  });

  /**
   * Clean styleguide assets folder.
   */
  gulp.task('styleguide:clean', () => {
     return plugins.del('./docs/styleguide/assets/**');
  });
};
