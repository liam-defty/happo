'use strict';

module.exports = (gulp, plugins, config) => {
  const paths = config.paths.docs;

  gulp.task('sassdoc', () => {
    const stream = plugins.sassdoc({
      'dest': paths.src + '/sassdoc',
      'theme': 'neat'
    });

    gulp.src(paths.sassdoc)
      .pipe(stream)
      .on('end', () => {
        console.log('Sass parsed');
      });

    return stream.promise.then(() => {
      console.log('Sass documentation complete');
    });
  });
};
