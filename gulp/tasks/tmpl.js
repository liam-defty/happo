'use strict';

var gulpHbs = require('gulp-compile-handlebars');
var rename = require('gulp-rename');
var path = require('path');
var data = require('gulp-data');

module.exports = (gulp, plugins, config) => {

  gulp.task('tmpl', function () {
    var options = {
      batch: ['./src/templates/partials'],
    };

    return gulp.src('./src/templates/*.hbs')
      .pipe(data(function(file) {
         return require('../../src/templates/data/' + path.basename(file.path.replace('.hbs', '')) + '.json');
      }))
      .pipe(gulpHbs({
        "img": '/assets/images/'
      }, options))
      .pipe(rename({
        extname: '.html'
      }))
      .pipe(gulp.dest('static'));
  });
}
