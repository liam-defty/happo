'use strict';

/**
 * Main dirs.
 */
const srcDir  = 'src';
const distDir = 'public';
const docsDir = 'docs';

/**
 * Globally accessible options.
 */
const config = {

  ENV: process.env.NODE_ENV = process.env.NODE_ENV || 'dev',

  // Tasks to use. Named as filenames in `gulp-tasks` folder
  tasks: [
    'browsersync',
    'clean',
    'docs',
    'favicons',
    'images',
    's3',
    'sassdoc',
    'serve',
    'static',
    'styles',
    'svg2png',
    'watch',
    'webpack',
    //'tmpl',
  ],

  // Base paths
  src:  srcDir,
  dist: distDir,

  // Task paths
  paths: {
    styles: {
      includePaths: Array.prototype.concat(
        require('bourbon').includePaths,
        require('node-neat').includePaths,
        require('node-normalize-scss').includePaths
      ),
      src: [
        `${srcDir}/styles/app.scss`
      ],
      lint: [
        './src/styles/**/*.scss',
        '!./src/styles/core/vendor/**/*.scss',
        '!./src/styles/core/utils/mixins/**/*.scss',
        '!./src/styles/core/utils/functions/**/*.scss',
      ],
      out: `${distDir}/assets/styles`
    },

    scripts: {
      entry: `${srcDir}/app/app.js`,
      src: [
        `${srcDir}/app/app.js`,
        `${srcDir}/app/modules/**/*.js`
      ],
      out: `${distDir}/assets/scripts`
    },

    bower: {
      out: `${srcDir}/assets/scripts/`
    },

    images: {
      src: `${srcDir}/images/**/*.{jpg,jpeg,png,gif,svg}`,
      svg: `${srcDir}/images/**/*.svg`,
      out: `${distDir}/assets/images`
    },

    static: {
      src: `${srcDir}/static/**`,
      out: `${distDir}/assets/`
    },

    livereload: {
      src: `${distDir}/**/*.{html,twig,php,css,js,map,png,jpg,jpeg,gif,svg}`
    },

    favicons: {
      src: 'src/favicons/index.html',
      srcDir: 'src/favicons',
      out: 'public/assets/favicons/',
      options: {
        files: {
          src: 'src/favicons/favicon.png',
          dest: '../../public/assets/favicons/',
          iconsPath: '/assets/favicons',
        },
        icons: {
          appleStartup: false,
        }
      }
    },

    watch: [
      { src: `${srcDir}/styles/**`, task: ['styles'] },
      { src: `${srcDir}/app/**`, task: ['webpack:build-dev'] },
      { src: `${srcDir}/images/**/*.{png,jpg,jpeg,gif,svg}`, task: ['images'] },
      { src: `${srcDir}/static/**`, task: ['static'] },
      { src: `${srcDir}/templates/**/*`, task: ['tmpl'] }
    ],

    docs: {
      src: docsDir,
      sassdoc: `${srcDir}/styles/**/*.scss`
    },

    clean: {
      dev: `${distDir}/assets`,
      production: [
        `${distDir}/assets/app`,
        `${distDir}/assets/styles`,
      ]
    },

    /**
     * S3 config.
     * 1. The local folder to send up
     * 2. Map to this folder on S3
     */
    s3: {
      local: `${distDir}/content/themes/bbwp/assets/**/*`, /* 1 */
      mapTo: `/content/themes/bbwp/assets/` /* 2 */
    }
  },

  // Used for LiveReload
  isServing: false,
  isBrowserSync: false,

  // Copyright banner
  banner: `Copyright (c) ${new Date().getFullYear()} Big Bite Creative | bigbitecreative.com | @bigbitecreative`
};

module.exports = config;
