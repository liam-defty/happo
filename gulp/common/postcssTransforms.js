var config = require('../config');

// PostCSS transforms
var transforms = [
  require('autoprefixer')({

    // Autoprefixer defaults, with the addition of IE > 10.
    browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'ie > 10']
  }),
  require('postcss-pxtorem')({
    prop_white_list: ['font', 'font-size', 'line-height', 'letter-spacing'],
  }),
  // require('colorguard'),
  require('postcss-pseudo-class-enter'),
  require('postcss-reporter')({
    clearMessages: true
  }),
];

// PostCSS production transforms
if (config.ENV === 'production') {
  transforms.push(require('cssnano')());
}

// Make sure banner is last
transforms.push(require('postcss-banner')({ banner: `! ${config.banner} ` }));

module.exports = transforms;

