var path = require('path');
var webpack = require('webpack');
var config = require('./gulp/config');

var webpackConfig = {
  cache: true,

  /**
   * Entry points.
   */
  entry: {
    bundle: path.join(__dirname, config.paths.scripts.entry),
    vendor: [
      'jquery',
      path.join(__dirname, 'src/app/lib/lodash.js')
    ]
  },

  /**
   * Output settings.
   */
  output: {
    path: path.join(__dirname, config.paths.scripts.out),
    publicPath: '/assets/', /* For webpack dev server. */
    filename: '[name].js'   /* [name] is the name of the entry. */
  },

  /**
   * Module loaders.
   */
  module: {
    loaders: [

      // Expose App to window.
      //{ test: /app\.js$/, include: /(src\/app)/, loader: 'expose?App' },

      // Expose jQuery and $ to window. Alternatively, use `require('expose?$!expose?jQuery!jquery');` in `app.js`.
      { test: /jquery\.js$/, loader: 'expose?jQuery!expose?$' },

      // Run Babel on .js/x.
      { test: /(\.js)|(\.jsx)$/, include: /(src\/app)/, loader: 'babel-loader?presets[]=es2015' },

      // Lint .js/x files with ESLint. Must come after Babel in loaders array.
      // { test: /(\.js)|(\.jsx)$/, include: /(src\/scripts)/, loader: 'eslint-loader' }
    ]
  },

  /**
   * Setup external scripts to use within webpack.
   */
  externals: {

    // Example including an extenal jQuery (it exports the global object `jQuery`), making it possible to `require('jquery');`:
    // 'jquery': 'jQuery',
  },

  /**
   * Plugins.
   */
  plugins: [

    // Resolve bower.json files.
    new webpack.ResolverPlugin(
      new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin('bower.json', ['main'])
    ),

    // Provide modules to be readily available (no need to import/require).
    new webpack.ProvidePlugin({
      '$': 'jquery',
      'jQuery': 'jquery',
    }),

    // Split entry points to their own bundle.
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor'],
      minChunks: Infinity,
    }),

    // Add banner to each bundle.
    new webpack.BannerPlugin(config.banner),

    // Define NODE_ENV. This has effect on the react lib size, and useful for environment specific code.
    // new webpack.DefinePlugin({
    //   'process.env': {
    //     'NODE_ENV': JSON.stringify('production')
    //   }
    // }),

    // Or enable NODE_ENV replacement, using `NODE_ENV=production` or `gulp --production`.
    new webpack.EnvironmentPlugin('NODE_ENV'),
  ],

  /**
   * ESLint config.
   */
  eslint: {
    configFile: '.eslintrc'
  },

  /**
   * Resolve config for Bower.
   */
  resolve: {
    root: [ path.join(__dirname, 'bower_components') ]
  }
};

/**
 * Production config.
 */
if (process.env.NODE_ENV === 'production') {
  webpackConfig.plugins.push(
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin()
  );

/**
 * Dev config.
 */
} else {
  webpackConfig.devtool = 'sourcemap';
  webpackConfig.debug = true;
}

module.exports = webpackConfig;
