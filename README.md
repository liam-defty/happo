# Happo
> Codebase for college project: Happo - A health app for children.

## Global requirements
- Node ^4.0.0
- gulp ^3.9.0
- Bower

## Documentation
### Environment variables
Set environment variables in the `.env` file.

### Webpack
We're using Webpack to build our JS, for several reasons. Firstly, it allows us to use ES6 modules in all their glory; secondly, it manages our dependencies/third-party libraries without us having to dump the source files into a static folder, or run separate Bower tasks to bundle them up.

You'll notice that there is a `webpack.config.js` file in the root; there's a lot of content in there, but is documented if you need to modify anything. To require a third-party library, install it via npm, if available, or Bower. Once that's done, you'll need to add it to the `vendor` array. To make the library available to your modules, import it into any modules that require it, or if it will be used a lot, you can automatically provide it to all modules by adding it to webpacks's `ProvidePlugin` object.

If you require the library to be available to the DOM, you will need to add it to the webpack loaders — examples are in the config. That's it!

#### Building webpack bundles.
Run `gulp webpack:build`, and your library will now be available for use. `gulp watch` will run `webpack:build-dev`, which will not Uglify code, so make sure to run `gulp webpack:build` before committing.

### jQuery
jQuery is included by default and is available to every module through the `$` object. Although we will try and avoid using it unless absolutely necessary. 

### PostCSS
We're using PostCSS to autoprefix, convert `px` to `rem` values for font properties and to minify CSS. Avoid using Sass to convert values / prefixing properties as we step towards handing off functions to PostCSS, enabling us to write in a more CSS-like way.

## Gulp tasks
### Core
- `default` - Runs styles, webpack:build, scripts, static, images.
- `build` - Cleans, then runs default task.

### Individual
- `browsersync` — Launches BrowserSync server, watches files for change. Use `--open` flag to open page in browser. Use `--noftify` flag to enable in-browser BowserSync notifications.
- `clean` - Cleans out the build files.
- `images` - Compresses `.{jpg,jpeg,png,gif,svg}` in `src/images/`
- `s3:publish` - **EXPERIMENTAL.** Publishes assets to s3 with gzipping.
- `serve` - Spin up a HTTP server with LiveReload. Use `--open` flag to open page in browser.
- `static` - Compile assets in `src/static` individually. Useful for fonts and single scripts etc.
- `styles` - Compile Sass from `src/styles/` and post process with PostCSS.
- `svg2png` - Convert .svg files in `src/images/` to `.png`.
- `watch` - Watch files for change, with LiveReload.
- `webpack:build-dev` - Webpack development build. Includes sourcemaps and no uglify. Used by `watch`.
- `webpack:build` - Webpack production build. Uglifies.

### Environment flags
Use the `--production` flag to run in production mode. This minifies Sass and uglifies JS. When using the `--production` flag, image tasks will be ignored.

### Server build
Run `npm run build` on the server to run server specific tasks in production mode. An alias for `gulp server-build --production`.
